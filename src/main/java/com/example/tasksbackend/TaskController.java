package com.example.tasksbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/tasks")
public class TaskController {
    @Autowired
    TaskRepository repo;

    @GetMapping("")
    public Iterable<Task> getTasks(){
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Task> getTask(@PathVariable Long id){
        return repo.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable Long id){
        repo.deleteById(id);
    }

    @PostMapping
    public ResponseEntity<?> addTask(@RequestBody Task newTask, UriComponentsBuilder uri) {
        if (newTask == null || newTask.getTitle() == null) {
            // Reject
            System.out.println("FAILURE line 38 " + newTask.toString());
            return ResponseEntity.badRequest().build();
        }
        // save new task
        // this operation assigns the task.id value
        newTask=repo.save(newTask);

        // create a URI pointing to the new task record
        URI location= ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}").buildAndExpand(newTask.getId())
                .toUri();

        // create the response
        ResponseEntity<?> response=ResponseEntity.created(location).build();
        return response;
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<?> putCustomer(@RequestBody Task newTask,
                                         @PathVariable("taskId") long taskId) {
        if (newTask.getId()!=taskId || newTask.getTitle()==null) {
            return ResponseEntity.badRequest().build();
        }
        repo.save(newTask);
        return ResponseEntity.ok().build();
    }
}
