package com.example.tasksbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TasksBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(TasksBackEndApplication.class, args);
    }

}
